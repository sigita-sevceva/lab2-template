import numpy as np
import statsmodels.api as sm


def backward_elimination(x, y, p_threshold=0.05, verbose=False):
    # Create fake intercept by adding column with ones
    x = np.append(arr=np.ones((np.shape(x)[0], 1)).astype(int),
                  values=x, axis=1)

    while True:
        # Fit the full model with all possible predictors
        regressor_ols = sm.OLS(endog=y, exog=x).fit()

        if verbose:
            print(regressor_ols.summary())

        remove_i = np.argmax(regressor_ols.pvalues)

        if regressor_ols.pvalues[remove_i] < p_threshold:
            return x

        if verbose:
            print('Remove column:', remove_i + 1)

        x = np.delete(x, remove_i, axis=1)
