#!/usr/bin/env python
"""Importing necessary packages"""
import numpy as np
from sklearn.datasets import load_boston
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor

import common.feature_selection as feat_sel
import common.test_env as test_env


def print_metrics(y_true, y_pred, label):
    """printing each function R squared"""
    print('%s R squared: %.2f' % (label, r2_score(y_true, y_pred)))


def linear_regression(x, y, print_text='Linear regression all in'):
    """Linear regression all in"""
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.25, random_state=0)

    reg = LinearRegression()
    reg.fit(x_train, y_train)
    print_metrics(y_test, reg.predict(x_test), print_text)
    return reg


def linear_regression_selection(x, y):
    """regression with backwards feature selection"""
    x_sel = feat_sel.backward_elimination(x, y)
    return linear_regression(x_sel, y, print_text='Linear regression with feature selection')


def polynomial_regression(x, y):
    """the relationship between the independent and dependent variables"""
    poly_feat = PolynomialFeatures(degree=2)
    x_poly = poly_feat.fit_transform(x)
    # Split train test sets
    x_train, x_test, y_train, y_test = train_test_split(
        x_poly, y, test_size=0.25, random_state=0)

    reg = LinearRegression()
    reg.fit(x_train, y_train)
    print_metrics(y_test, reg.predict(x_test), 'Polynomial regression')
    return reg


def support_vector_regression(x, y):
    """for SVR we scale features and dependent variable"""
    sc = StandardScaler()
    x = sc.fit_transform(x)
    y = sc.fit_transform(np.expand_dims(y, axis=1))

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.25, random_state=0)

    reg = SVR(kernel='rbf', gamma='auto')
    reg.fit(x_train, np.squeeze(y_train))
    print_metrics(np.squeeze(y_test), np.squeeze(reg.predict(x_test)), 'SVR')
    return reg


def decision_tree_regression(x, y):
    """assigning a random state=0 to keep same values for each run"""
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.25, random_state=0)
    reg = DecisionTreeRegressor(random_state=0)
    reg.fit(x_train, np.squeeze(y_train))
    print_metrics(y_test, reg.predict(x_test), 'Decision tree regression')
    return reg


def random_forest_regression(x, y):
    """creating a multitude of decision trees, using 10 n_estimators"""
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.25, random_state=0)
    reg = RandomForestRegressor(n_estimators=10)
    reg.fit(x_train, y_train)
    print_metrics(y_test, reg.predict(x_test), 'Random forest regression')
    return reg


if __name__ == '__main__':
    test_env.versions(['numpy', 'statsmodels', 'sklearn'])

    # https://scikit-learn.org/stable/datasets/index.html#boston-house-prices-dataset
    x, y = load_boston(return_X_y=True)

    linear_regression(x, y)
    linear_regression_selection(x, y)
    polynomial_regression(x, y)
    support_vector_regression(x, y)
    decision_tree_regression(x, y)
    random_forest_regression(x, y)

    print('Done')
    print('\n')
